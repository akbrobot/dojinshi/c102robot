# レゴブロックで作ったエレベーターの組立図 書いてみた　その1　フロア編
//raw[|latex| \begin{center} ]
熊秀創吉  *熊吉らぼ*
//raw[|latex| \end{center} ]
## フロア編
### 全体
![](images/dd43f5c3-60ee-0cc0-f952-4d917e0bce99.png)
![](images/5910930c-705c-5547-fa48-5ef2cccab83e.png)
![](images/9f2fc955-5c17-af90-8c42-0dec6868104a.png)
![](images/26c5c838-fd27-2344-ae9e-13296d6863cb.png)

### 小分け分解
![](images/5681bc3e-7626-9335-178c-bba91bbfb4ee.png)
![](images/e18300f1-1249-a010-d229-271c2087857b.png)

![](images/6a0612a4-2a1f-96db-1a42-b8ca06f781ed.png)
![](images/0a3f7504-4a24-2b53-6364-37d9776cd944.png)

![](images/5c0edcdd-e6c9-db5f-ccce-c4ed317103cc.png)

![](images/7887f27f-8a0d-96d7-de62-8a961c240e18.png)

![](images/06dbb016-3ff5-7e39-9fa4-2b58ccf409da.png)

![](images/5ea4612a-84bf-4579-eeec-eb63082b73a9.png)

#### 最後に
部品名等については小さ過ぎるためこの冊子には記載しておりませんが、
「秋葉原ロボット部」のサイトにて公開したいと思います。
詳細はこちら　https://akbrobot.gitlab.io/blockelevator/

