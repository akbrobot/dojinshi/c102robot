
= レゴブロックで作ったエレベーターの組立図 書いてみた　その１　カゴ編


//raw[|latex| \begin{center} ]
熊秀創吉  @<b>{熊吉らぼ}
//raw[|latex| \end{center} ]


== カゴ編

=== 全体


//indepimage[9fc499d0-0436-f733-c889-09c392290619]




//indepimage[09f576fb-2fca-9415-b752-95b9a1e493da]

//indepimage[32d8b28e-98f5-70a0-f5cb-9324b878b367]



=== 小分け分解


//indepimage[532d8615-56f2-b4cf-fed0-75df5be02e5f]




//indepimage[d69e228f-4b86-ae63-0ad3-a2ed4390ada7]

//indepimage[cbf3727e-7de3-88b2-6742-d069b16f8199]



=== 機構部
 * 扉センサー取付のテクニックプレートはVR取付の為、削ります。
 * ツマミ先端にレゴのシャフト用十字孔を開けます。



//indepimage[cd25772b-33c8-d855-350c-50d0f6254ca2]

//indepimage[b3ee6dad-62e8-c575-c461-edd5a8f3b308]

//indepimage[195007f8-39e3-e608-3b84-da0efea3e176]




//indepimage[c552b681-5cca-fc5b-2bde-babdd00e5594]




//indepimage[a41a7cbf-4fce-667d-bdf0-8e4183524dcf]




//indepimage[ad9b51a6-8043-16fc-850a-df18e5d46e58]



==== 最後に


部品名等が小さ過ぎるためこの冊子には記載しておりませんが、
「秋葉原ロボット部」のサイトにて公開したいと思います。
詳細はこちら　https://akbrobot.gitlab.io/blockelevator/

