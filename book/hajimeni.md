

# はじめに

## この冊子について

秋葉原支部ロボット部において、Qiita Engineer Festa 2023 で発表した内容などをまとめています。

## Qiita での公開

 原稿は、Qiita で公開した記事を、ソーシャル組版システム「Qiita2Review 」を使い製版、製本しています。

元記事は、インターネット上で参照できます。


### 総力特集「時空を超えた相対性理論が超常現象の謎を解き明かす！」
- 「読書会でベクトル解析の『回転』で躓いたので整理してみました」
 - 秋葉原ロボット部 たいちゃん
 - https://qiita.com/toshita172/items/b377feac423215560c6d

-「ポアソン方程式ってナンだ？」
 - 秋葉原ロボット部 部長 清水 素釘武
 - https://qiita.com/tshimizu8/items/fc801072db10ae03c08f

-「SageMathで波動方程式の一般解を求める」
 - うえだ・ふぁくとりー かずえだ
 - https://qiita.com/kazueda/items/f00861254723c617fa49

-「ブラックホールによる重力レンズをシミュレーション」
 - 秘密結社オープンフォース　河野悦昌
 - https://qiita.com/nanbuwks/items/97b8c2186850b5b73e2d

### 第2特集「マヨイガと現実がつながるとき、人類に最後の審判が訪れる!」
-「zigbee写真伝送のフィールドテスト」
 - 秋葉原ロボット部 @shimiken
 - https://qiita.com/kenshimizu246/items/3459a11fc30f2e4433b1

-「お父さん、明日からVtuberで食べていこうと思うんだ。（ドラフト版）」
 - 只の人 @hichon
 - https://qiita.com/hichon/items/c01af81e89373dcb2a8e 

-「福島第一原発　視察レポート」
 - 秋葉原ロボット部/ブラック企業診断士 武装猛牛
 - https://qiita.com/busyoucow/items/54c7ff3aa1b5814d3af6

### 豪華巻末付録
- 「 レゴブロックで作ったエレベーターの組立図 書いてみた その1 フロア編」
 - 秋葉原ロボット部　熊秀創吉（熊吉らぼ）
 - https://qiita.com/KumahideSoukichi/items/e214479017dbecbb03d7

-「『一般相対性理論を一歩一歩数式で理解する』 私家版索引 」
 - 秋葉原ロボット部　
 - https://gitlab.com/akbrobot/reading_relativity/-/wikis/home

## ライセンスについて
個別に指定したもの以外は Public Domain (CC0)扱いです。Qiitaに公開した記事ともども、ご自由にご使用下さい。


