
= PNG に編集ソースファイルを埋め込んで再編集可能にする その1


//raw[|latex| \begin{center} ]
河野悦昌  @<b>{秘密結社オープンフォース}
//raw[|latex| \end{center} ]
「PNG にテキストを埋め込む」
https://qiita.com/nanbuwks/items/969642abde48bb272c25



では、png に drawio のソースが埋め込まれていることを示しました。



「PNG Specification: Chunk Specifications」
https://www.w3.org/TR/PNG-Chunks.html
の説明では、tExt チャンクにはキーワードがついていて、そのキーワードについては以下のようにあります。


//emlist{
The keyword indicates the type of information represented by the text string. The following keywords are predefined and should be used where appropriate:

   Title            Short (one line) title or caption for image
   Author           Name of image's creator
   Description      Description of image (possibly long)
   Copyright        Copyright notice
   Creation Time    Time of original image creation
   Software         Software used to create the image
   Disclaimer       Legal disclaimer
   Warning          Warning of nature of content
   Source           Device used to create the image
   Comment          Miscellaneous comment; conversion from
                    GIF comment
For the Creation Time keyword, the date format defined in section 5.2.14 of RFC 1123 is suggested, but not required [RFC-1123]. Decoders should allow for free-format text associated with this or any other keyword.
Other keywords may be invented for other purposes. Keywords of general interest can be registered with the maintainers of the PNG specification. However, it is also permitted to use private unregistered keywords. (Private keywords should be reasonably self-explanatory, in order to minimize the chance that the same keyword will be used for incompatible purposes by different people.)
//}


drawio の埋め込みでは、規定のキーワードではないmxfile キーワードが使われているようです。



//indepimage[702a3ad5-f628-91f0-f34b-9e50d52c723c]




ここでは、独自に @<tt>{Embedded Source} キーワードを作ってしまいます。


== Embedded Source フォーマット


以下のようにしてフォーマットを規定します

//table[tbl1][]{
種別	長さ(Byte)	内容
-----------------
キーワード	15	Embedded Source
セパレータ	1	NULL文字
コンテンツ	可変	JSONデータ
//}


JSONデータの内訳は以下のようになっています。
|キー|データ|
|:---:|:---:|
|type|ファイルタイプを示す文字列|
|filename|ファイル名(パス無)|
|file|元ファイルをBASE64エンコードした文字列|



type は現在ファイル名の拡張子を入れています。
file の BASE64エンコードは RFC2045 (MIME) に基づいた76バイトごとに改行されたものとなります。
この他に mimetype が入る予定です。


== Drawファイルを作る


以下のようなDrawファイルを作ります。



//indepimage[33269f34-0d76-ff6d-3f35-123aaef2536a]




ここから、先の規定に基づいて png を作る拡張を作りました。(開発中です。)
//indepimage[fe55dcb6-ccd1-96ce-5f6e-adef9e056816]




現在は、ヘルパーを実行する必要がありますが、ヘルパーを実行するとファイルが表示されるので、
//indepimage[4edd1983-9cc3-74e8-154b-fadfa9bffa45]




これを Qiita に貼ります。



このようにして貼った png ファイルです。



//indepimage[27ad0741-5219-3000-1fe1-972ee92b8160]




この png ファイルを書き直したいな!



これをダウンロードします。



https://camo.qiitausercontent.com/14e30a975b5bdb10ca3b7d6a3442c72d20f5aebc/68747470733a2f2f71696974612d696d6167652d73746f72652e73332e61702d6e6f727468656173742d312e616d617a6f6e6177732e636f6d2f302f3133393532342f32376164303734312d353231392d333030302d316665312d3937326565393262383136302e706e67



開発中ですが、ヘルパーアプリを実行すると、アプリが開き、再現します。
//indepimage[dd91f9dc-9540-d4cc-d781-2abb223262cf]




再編集ができます。
//indepimage[475180c7-d173-ff31-b6e9-9c7781402f23]




開発中のヘルパーアプリはその2以降で解説します。

