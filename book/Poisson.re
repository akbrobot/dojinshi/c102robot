
= ポアソン方程式ってナンだ？


//raw[|latex| \begin{center} ]
清水 素釘武  @<b>{秋葉原ロボット部}
//raw[|latex| \end{center} ]


== 概要


ポアソン方程式は、スカラーポテンシャルを持つベクトル場の発散（湧き出し）に関する方程式と解釈できる。
例えば、重力ポテンシャルのポアソン方程式は、質量分布に従って、重力場に湧き出しがあることを示している。
湧き出しが時刻により動的に変化する場合はポアソン方程式でななく、波動方程式になる。


== はじめに


意識のモデリングに興味がある。
そのためにまず物理学でのモデリング知識を得ることにして、つぎの読書会を行っている。



「一般相対性理論を一歩一歩数式で理解する」 読書会@<fn>{site}
//footnote[site][https://akbrobot.connpass.com/event/275328/]



この中でいきなり「ポアソン方程式」が出てくる。 方程式の直感的な意味が不明で、個人的に感覚に接地せず、記号接地問題に直面した。
そこで調べるとポアソン方程式は、スカラーポテンシャルを持つベクトル場の発散として解釈できることがわかり、当たり前の主張をしている方程式であると納得でき、感覚に記号接地できた。
ポアソン方程式は電磁気学、移動現象論、流体力学といった物理学の諸領域において、系を記述する基礎方程式として現れる。
時間に依存性しない定常状態を記述する方程式はポアソン方程式となる。


== ポアソン方程式の定義


スカラー場@<m>{f(x)}よりスカラー場@<m>{\phi (x)}を求める、次の方程式をポアソン方程式という。
//texequation{
{\Delta \phi (x) = -f(x)}
//}


スカラー場@<m>{\Delta  \phi} ラプラシアンを展開すると、つぎのようになる。
//texequation{
{\displaystyle \Delta \phi =\frac{\partial ^2 \phi }{\partial {x^2}} +\frac{\partial ^2 \phi }{\partial {y^2}} + \frac{\partial ^2 \phi }{\partial {z^2}} }= \nabla  \cdot  \nabla \phi = \nabla ^2 \phi
//}


== ポアソン方程式をベクトル場で書き換える


（定理1.23）
@<m>{\phi(x)} を@<strong>{スカラーポテンシャル}、@<m>{A(x)}をベクトル場とすると、
//texequation{
{A(x)=- \nabla}{\phi(x)}
//}
但し直方体の領域@<m>{R}全体で、
//texequation{
{rot}{A(x)=0}
//}



ポアソン方程式をスカラーポテンシャルを持つベクトル場で書き換えると
//texequation{
{\displaystyle \Delta \phi(x) = \nabla  \cdot  \nabla \phi(x) = - \nabla  \cdot  A(x)=- f(x)}
//}
@<b>{ポアソン方程式は、スカラーポテンシャルを持つベクトル場の発散（湧き出し）の方程式である。}


== ポアソン方程式の例

=== 重力ポテンシャル


重力ポテンシャル @<m>{\phi (x)} は次のポアソン方程式を満たす。ここで@<m>{ρ(x)} を与えられた質量分布、@<m>{G}を万有引力定数とする。
//texequation{
{\Delta \phi (x) =4\pi G\rho (x)}
//}
==== 解釈
スカラー場 @<m>{-4\pi G\rho (x)} が、重力ポテンシャル @<m>{\phi (x)}とそれに付随する重力場を定めることを示している。つまり重力場
//texequation{
{A(x)=- \nabla \phi(x)}
//}



重力場 @<m>{A(x)}の発散 @<m>{\nabla \cdot A(x)} で書き直すと、



//texequation{
{\Delta \phi (x) =  \nabla \cdot \nabla \phi(x) = - \nabla \cdot A(x)       =4\pi G\rho (x)}
//}


これは、質量分布@<m>{\rho (x)}に従って、重力場@<m>{A(x)}に湧き出しがあることを示している。


=== 静電ポテンシャル


静電ポテンシャル @<m>{\phi (x)} は次のポアソン方程式を満たす。ここで@<m>{ρ(x)} を与えられた電荷分布、@<m>{\varepsilon _0}を真空の誘電率とする。
//texequation{
{{\displaystyle \Delta \phi (x) =- \frac{ρ(x)}{\varepsilon _0}}}
//}


==== 解釈

スカラー場 @<m>${- \frac{ρ(x)}{\varepsilon _0}}$ が、静電ポテンシャル @<m>{\phi (x)}とそれに付随する静電場を定めることを示している。つまり静電場
//texequation{
{A(x)=- \nabla \phi(x)}
//}


静電場 @<m>{A(x)}の発散 @<m>{\nabla \cdot A(x)} で書き直すと、



//texequation{
{\Delta \phi (x) =  \nabla \cdot \nabla \phi(x) = - \nabla \cdot A(x)       =\frac{ρ}{\varepsilon _0}}
//}



これは、電荷分布@<m>{ρ(x)}に従って、静電場@<m>{A(x)}に湧き出しがあることを示している。



====点電荷の場合
原点においた点電荷@<m>{q}がつくる静電ポテンシャルは、つぎのようになる。
//texequation{
{{\displaystyle \phi (r)  =\frac{q}{4 \pi \varepsilon _0} \frac{1}{r}}}
//}


これはクローンの法則より静電場@<m>{E(r)}は、つぎのようになり
//texequation{
{{\displaystyle E(r)  =\frac{q}{4 \pi \varepsilon _0} \frac{r}{r^3}}}
//}



静電ポテンシャルから静電場を計算すると
//texequation{
{{\displaystyle - \nabla \phi(r)=-\frac{q}{4 \pi \varepsilon _0}  \nabla \frac{1}{r} = \frac{q}{4 \pi \varepsilon _0} \frac{r}{r^3} = E(r)}}
//}



==== 任意の位置に置いた点電荷がつくる静電ポテンシャル
@<m>{r_1}においた点電荷@<m>{q_1}がつくる静電ポテンシャルは、
//texequation{
{{\displaystyle \phi(r)= \frac{1}{4 \pi \varepsilon _0} \frac{q_1}{|r-r_1|} }}
//}



==== 連続的な電荷分布の場合

//texequation{
{\displaystyle \phi(r)= \frac{1}{4 \pi } \int_V  \frac{ρ(r_1)}{\varepsilon _{0}} \frac{1}{|r-r_1|} dV }
//}

== 定理

=== @<strong>{（定理1.39）} ポアソン方程式の特殊解


@<m>{R^3}の領域@<m>{V}と、その外で@<m>{0} になる関数@<m>{f(x)} に対して、
//texequation{
{{\displaystyle \phi (x) = \frac{1}{4\pi}\int_{V}^{} \frac{f(y)}{\left|  y-x \right|} dy}}
//}
とおくと次の方程式を満たす。
@<m>{\Delta \phi (x) = -f(x)}



ここで 
//texequation{
x=(x_1,x_2,x_3)、y=(y_1,y_2,y_3)、dy=dy_1dy_2dy_3
//}
==== 解釈
ポテンシャル解 @<m>{\phi}は、［湧きだし密度］、［電荷密度］、［質量密度］などの空間的な分布の様子が時間的に変化しない場合ポアソン方程式を満足する。

 * 真空中で電荷密度ρ（ｒ）の分布がつくる静電電位場
 * 真空中に分布する質量密度ρ（ｒ）の分布がつくる重力ポテンシャル場等々がこの式を満足する。


=== @<strong>{（定理1.40）} 波動方程式（特殊解）


領域@<m>{V}と、その外で@<m>{0} になる位置 @<m>{x} と時刻 @<m>{t}の関数@<m>{f(x,t)} に対して、
//texequation{
{{\displaystyle \phi (x,t) = \frac{1}{4\pi}\int_{V}^{} \frac{f(y,t\pm \frac {{{\left|  y-x \right|}}}{c})}{\left|  y-x \right|} dy}}
//}


とおくと次の方程式を満たす。
//texequation{
{{\displaystyle (\Delta -  \frac{1}{c^2} \frac{\partial^2 }{\partial t^2} )\phi (x,t) = -f(x,t)}}
//}


==== 解釈

ポテンシャルを生み出す原因となる密度分布の様子が時間的に変化する（例えば湧きだしの強度が時間的に変化したり、電荷や質量が時間的に場所を移動する）場合には、“ポアソン方程式”を満足せず、“波動方程式”となる。



速度 @<m>{c}を無限大にすると、波動方程式は次のようにポアソン方程式になる。
//texequation{
{{\displaystyle \Delta \phi (x,t) = -f(x,t)}}
//}

== おわりに


言語において抽象度が高い単語（わび・さび、国家、資本主義など）は個人の感覚に接地しずらい。
すでに、感覚に記号接地している単語をとおして、これらの単語は間接的に感覚に記号接地すると考えられる。
もともと抽象的概念を扱う数学も感覚的に記号接地しずらい。数学的概念が直感的に理解できない時はどうすればよいか？具体的な作業としては、つぎが考えられる

 * 具体的な例を作成して考える
 * 既に感覚に記号接地している（数学）概念で解釈して、記号接地を目指す。
 * 抽象度を上げたり、下げたりして考える
 * 証明をなぞる、証明などを写経したりして、手を動かす。
 * ネットで検索してみる。わかりやすいページや説明動画を見つける
 * 関連する概念から理解する（周辺から攻める）
 * 放置する（いつか自然とわかる時がやってくる。時が解決してくれる。）
 * 人やネットに質問する



小学５年の生徒の半分は「１／２」と「１／３」のどちらが大きな数かわからず、分数の概念が感覚に記号接地できていないようである。
ChatGPTは感覚がなく、感覚に記号接地していなと考えられるが、ChatGPTの応答文は意味を理解しているように見える。
ChatGPTは、記号間の統計的な出現確立のみで、応答文を生成している。
ChatGPTは、ポアソン方程式をどのように理解しているのだろう？


== 付録 スカラーポテンシャル

=== スカラーポテンシャルの定義


（定理1.23）ベクトル場 @<m>{A(x)} が、直方体の領域@<m>{R}全体で、@<m>{rot} @<m>{A(x)=0} となるとき、スカラー場 @<m>{f(x)} が存在して、
@<m>{A(x)=-grad} @<m>{f(x)=- \nabla} @<m>{f(x)} と表される。
@<m>{f(x)} を@<strong>{スカラーポテンシャル}という。


=== 重力ポテンシャル


スカラーポテンシャルの例として重力ポテンシャルがある。
重力ポテンシャルは単位質量当たりの重力ポテンシャルエネルギーである。



@<m>{A(x)} を重力ベクトル場、@<m>{f(x)} を重力ポテンシャルとする。\newline
@<m>{g} を重力加速度、@<m>{e3 = (0, 0, 1)} を鉛直上向きの単位ベクトルとする。このとき



@<m>{A(x) = −ge3} である。また、@<m>{A(x) = −∇(gx3)} と書けるので、@<m>{f(x)=gx3} となる。


== 参考資料

=== 参考書籍


 * 一般相対性理論を一歩一歩数式で理解する 2017 石井俊全
 * 言語の本質 ことばはどう生まれ、進化したか 2023 今井むつみ,秋田喜美


=== 参考URL

//emlist{
ポアソン方程式（Poisson's equation）と波動方程式（wave equation）ＦＮの高校物理
http://fnorio.com/0181Poisson@<b>{equation/Poisson}equation.html

非同次波動方程式の一般解 ＦＮの高校物理
http://fnorio.com/0122inhomogeneous@<b>{wave}equation0/inhomogeneous@<b>{wave}equation0.html

逆二乗力（重力場、電磁場）のポテンシャルがラプラス方程式を満たすことの証明
https://math-fun.net/20211129/20608/

ポアソン方程式 大学物理のフットノート
https://diracphysics.com/portfolio/physicalmath/S4/pPoisson.html

静電ポテンシャルの基本事項 大学物理のフットノート
https://diracphysics.com/portfolio/electromagnetism/S2/escalerpotential.html

静電場：ポアソン方程式の解
https://home.hirosaki-u.ac.jp/relativity/電磁気学 I/静電場：ポアソン方程式の解/

2階偏微分方程式の分類：楕円型、放物型、双曲型とは
https://math-fun.net/20211009/19243/

古典的な物理学に現れる調和関数—物理的現象を記述する関数の1つ
https://www.kyoto-su.ac.jp/project/st/st18_01.html#:~:text=ある関数を変数ごと,ツールになっています。
//}

=== 参考YouTube

//emlist{
ポアソン方程式と重力場の方程式
https://www.youtube.com/watch?v=-mpAYUKx2Bo&ab_channel=ようつべ先生の数学教室
//}

=== 参考ウィキペディア


 * https://ja.wikipedia.org/wiki/ポアソン方程式
 * https://ja.wikipedia.org/wiki/ラプラス方程式   調和関数
 * https://ja.wikipedia.org/wiki/重力ポテンシャル
 * https://ja.wikipedia.org/wiki/万有引力
 * https://ja.wikipedia.org/wiki/楕円型偏微分方程式
 * https://ja.wikipedia.org/wiki/楕円
 * https://ja.wikipedia.org/wiki/円錐曲線

