
= お父さん、明日からVtuberで食べていこうと思うんだ。（ドラフト版）


//raw[|latex| \begin{center} ]
hichon  @<b>{只の人}
//raw[|latex| \end{center} ]


== 用意するもの
 * パソコン － ゲーミングPC程ではないがそれなりのスペックが必要になる。
 * Webカメラ － Vtubeのモデルをフェイストラッキングで動かすのに使用する。よくわからなければ、ロジクールの2万円ぐらいのWebカメラ買えば良い。ノートパソコンのカメラを使用する事もできる。または、スマートフォンのカメラを代用することもできる。
 * ヘッドセット － パソコンにスピーカーとマイクが付いていれば無くても良いが、ボイスチェンジャーを使うならあった方が良い。
 * Steamクライアント － 以下のソフトがSteamで無料配布されているので、インストールに使用する。
 * VTube Studio － フェイストラッキングソフト。カメラに映る自分の動きに合わせてLive2dモデルをぬるぬる動かすことができる。
 * OBS Studio － オープンソースのストリーミング配信・録画ソフト。VTube Studioの表示を仮想カメラに出力して、ZoomなどのWeb会議ソフトの入力に流し込める。


== 環境


とりあえず、以下の手持ちの環境で試してみた。


=== その1
 * Windows マシン
 * OS：Windows 10 Pro (22H2)
 * CPU：AMD Ryzen 7 3700X 8-Core Processor 3.60 GHz
 * Memory：48 GByte
 * Webカメラ：Google Pixel 7 のカメラを使用
 * ヘッドセット： RAZER BLACKSHARK V2


=== その2
 * Macbook Pro (13 inch, 2020, Four Thunderbolt 3 ports)
 * OS：macOS Ventura (13.5)
 * CPU：2 GHz Quad Core Intel Core i5
 * Memory：16 GByte
 * Webカメラ：パソコン本体のカメラを使用
 * ヘッドセット： RAZER BLACKSHARK V2


== 各種ソフトのインストール
 * まずは、Steamのサイト[^1](https://store.steampowered.com/about/)で「Steamをインストール」をクリックして、Steamクライアントをインストールする。
 * Steamクライアントで「VTube Studio」を検索して、インストールする。
 * Steamクライアントで「OBS Studio」を検索して、インストールする。


== VTube Studioのセットアップ


Steamクライアントの[ライブラリ]からVTube Studioを起動する。



公式サイト[^2](https://denchisoft.com/)の「Documentation」から公式マニュアル[^3](https://github.com/DenchiSoft/VTubeStudio/wiki)に移動して、Getting Started[^4](https://github.com/DenchiSoft/VTubeStudio/wiki/Getting-Started)にしたがってセットアップする。


== OBS Studioのセットアップ


Steamクライアントの[ライブラリ]からOBS Studioを起動する。



公式サイト[^5](https://obsproject.com/ja)の「Knowledge Base」のGetting Started[^6](https://obsproject.com/kb/category/1)にしたがってセットアップする。



簡単に説明すると

 * 「ソース」ペインで［＋］(ソース追加)ボタンをクリックし「ゲームキャプチャ」を追加する。
 * 「モード」で「特定のウインドウをキャプチャ」を選択し、「ウィンドウ」で「VTube Studio」を選択する。
 * これでメイン画面にVTube Studioの画面が表示されるので、「コントロール」ペインの「仮想カメラ開始」をクリックする。


== ZoomなどのWeb会議でモデルを表示する。
 * 設定画面などにビデオ、または、カメラの設定があるので入力先としてOBS Studioの仮想カメラを選択する。
 * これで、ビデオ会議等でぬるぬる動くモデルを表示する事ができます。



簡単ですが、今回はここまでとします。

